using Microsoft.AspNetCore.Mvc;

List<Book> books = new List<Book>();

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

WebApplication app = builder.Build();

app.MapGet("/books/all", () =>
{
    return Results.Ok(books);
}
);

app.MapPost("/books", ([FromBody] Book book) =>
{
    book.Id = Guid.NewGuid();
    books.Add(book);
    return Results.Ok(books);
}
);

app.MapGet("/books/ById/{id}", (Guid id) =>
{
    Book book = books.FirstOrDefault(x => x.Id == id);
    return Results.Ok(book);
}
);

app.MapPut("/books/Update/{id}", (Guid id, [FromBody] Book updateBook) =>
{
    Book book = books.FirstOrDefault(b => b.Id == id);
    book.Title = updateBook.Title;
    book.Author = updateBook.Author;
    return book;
}
);

app.MapDelete("/books/Delete/{id}", (Guid id) =>
{
    Book book = books.FirstOrDefault(b => b.Id == id);
    books.Remove(book);
    return book;
}
);


app.Run();

public class Book
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }
}